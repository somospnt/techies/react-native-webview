import React from 'react';
import { Button, Overlay, Icon } from '@rneui/themed';
import { Text, StyleSheet } from 'react-native';

const Alerta = (props) => {
    return (
        <Overlay isVisible={props.visible} onBackdropPress={props.toggleOverlay}>
            <Text style={styles.textPrimary}>Alerta</Text>
            <Text style={styles.textSecondary}>
                {props.mensaje}
            </Text>
            <Button
                icon={
                    <Icon
                        name="wrench"
                        type="font-awesome"
                        color="white"
                        size={25}
                        iconStyle={{ marginRight: 10 }}
                    />
                }
                title="Reintentar"
                onPress={props.toggleOverlay}
            />
        </Overlay>
    );
};

const styles = StyleSheet.create({
    button: {
        margin: 10,
    },
    textPrimary: {
        marginVertical: 20,
        textAlign: 'center',
        fontSize: 20,
    },
    textSecondary: {
        marginBottom: 10,
        textAlign: 'center',
        fontSize: 17,
    },
});

export default Alerta;