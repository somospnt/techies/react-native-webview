import { WebView } from "react-native-webview";
import React, { useRef, useState, useEffect } from "react";
import { getData, storeData } from "../utils/AsyncStorage";
import { BackHandler } from 'react-native';
import Alerta from "./Alerta";

export default function WebViewLogin({ navigation }) {
    const webviewRef = useRef(null);
    const [visible, setVisible] = useState(false);
    const [mensaje, setMensaje] = useState('');
    const [isCuentaCreada, setIsCuentaCreada] = useState(false);

    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", handleBackButtonPress)
        return () => {
            BackHandler.removeEventListener("hardwareBackPress", handleBackButtonPress)
        };
    }, []);

    const handleBackButtonPress = () => {
        webviewRef.current?.goBack();
        return true;
    };

    const mostrarAlerta = (mensaje) => {
        setMensaje(mensaje);
        setVisible(!visible);
        isCuentaCreada && enviarMensajeAWebView({ event: 'redirect', payload: 'login' });
    };

    const recibirMensajeDeWebView = async (eventWebView) => {
        const { event, payload } = JSON.parse(eventWebView.nativeEvent.data);
        await eventosDelPostMessage(event, payload);
    };

    async function eventosDelPostMessage(event, payload) {
        setIsCuentaCreada(false);
        if (event == 'login') {
            const storage = await getData();
            if (storage.email === payload.email &&
                storage.password === payload.password) {
                navigation.navigate('Perfil')
            } else {
                const mensaje = 'Credeciales invalidas';
                mostrarAlerta(mensaje);
            }
        }
        if (event == 'crearCuenta') {
            mostrarAlerta('Credenciales guardadas con éxito');
            storeData(payload);
            setIsCuentaCreada(true);
        }
        if (event == 'alerta') {
            mostrarAlerta(payload.mensaje);
        }
    };

    function enviarMensajeAWebView(mensaje) {
        webviewRef.current?.injectJavaScript(
            getInjectableJSMessage(mensaje)
        );
    }

    function getInjectableJSMessage(message) {
        return `
      (function() {
        document.dispatchEvent(new MessageEvent('message', {
          data: ${JSON.stringify(message)}
        }));
      })();
    `;
    }

    return (
        <>
            <WebView
                source={{ uri: "http://localhost:3000" }}
                onMessage={recibirMensajeDeWebView}
                injectedJavaScript={getInjectableJSMessage('Desde la inyección de javascript')}
                ref={webviewRef}
            />
            <Alerta toggleOverlay={mostrarAlerta} visible={visible} mensaje={mensaje} />
        </>
    );
}
