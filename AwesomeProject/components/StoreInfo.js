import React, { useEffect, useState } from 'react'
import { getData, storeData } from '../utils/AsyncStorage';
import {
    TouchableOpacity,
    Text,
    View,
} from "react-native";

function StoreInfo() {
    const [data, setData] = useState({});

    useEffect(() => {
        cargarDatos();
    }, []);

    useEffect(() => {
        storeData(data);
    }, [data]);

    const cargarDatos = async () => {
        const storage = await getData();
        setData(storage);
    };

    return (
        <View style={{ backgroundColor: 'yellow' }}>
            <Text style={{ color: "black" }}>{`Datos en el storage: ${JSON.stringify(data)}`}</Text>
            <TouchableOpacity
                style={{ backgroundColor: "green", borderRadius: 1 }}
                onPress={async () => {
                    const data = await getData();
                    setData(data);
                }}
            >
                <Text>Cargar Datos</Text>
            </TouchableOpacity>
        </View>
    )
}

export default StoreInfo;