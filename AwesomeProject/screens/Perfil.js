import React, { useEffect, useState } from 'react'
import { Text, View } from 'react-native';
import { Avatar } from '@rneui/themed';
import { getData } from '../utils/AsyncStorage';

function Perfil() {
    const [nombre, setNombre] = useState('');

    useEffect(() => {
        obtenerNombre();
    }, []);

    const obtenerNombre = async () => {
        const storage = await getData();
        setNombre(storage.nombre)
    };

    return (
        <View>
            <Avatar
                size={70}
                rounded
                icon={{ name: "pencil", type: "font-awesome" }}
                containerStyle={{ backgroundColor: "#9700b9" }}
            />
            <Text style={{ fontSize: 20 }}>Bienvenide {nombre} a esta App Asombrosa</Text>
        </View>
    )
}

export default Perfil