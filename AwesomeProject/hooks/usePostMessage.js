import { useRef } from "react";
import { getData, storeData } from "../utils/AsyncStorage";

const usePostMessage = () => {
    const webviewRef = useRef(null);

    const recibirMensajeDeWebView = async (eventWebView) => {
        const { event, payload } = JSON.parse(eventWebView.nativeEvent.data);
        await eventosDelPostMessage(event, payload);
    };

    async function eventosDelPostMessage(event, payload) {
        if (event == 'login') {
            const storage = await getData();
            if (storage.email === payload.email &&
                storage.password === payload.password) {
                alert(`Bienvenido ${storage.nombre}`);
            } else {
                const mensaje = 'Credeciales invalidas';
                enviarMensajeAWebView(mensaje);
            }
        }
        if (event == 'crearCuenta') {
            alert("Credenciales guardadas con éxito");
            storeData(payload);
        }
    };

    function enviarMensajeAWebView(mensaje) {
        webviewRef.current?.injectJavaScript(
            getInjectableJSMessage(mensaje)
        );
    }

    function getInjectableJSMessage(message) {
        return `
      (function() {
        document.dispatchEvent(new MessageEvent('message', {
          data: ${JSON.stringify(message)}
        }));
      })();
    `;
    }
    return {
        webviewRef,
        recibirMensajeDeWebView,
        enviarMensajeAWebView,
        getInjectableJSMessage
    }
}

export default usePostMessage;