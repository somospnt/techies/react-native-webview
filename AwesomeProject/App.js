import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import {
  SafeAreaView,
  StatusBar,
} from "react-native";
import StorageInfo from './components/StoreInfo';
import WebViewLogin from "./components/WebViewLogin";
import Perfil from "./screens/Perfil";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <SafeAreaView style={{ flex: 1 }}>
        <StatusBar style='light' backgroundColor='#2f3848' />
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen name="Login" component={WebViewLogin} />
          <Stack.Screen name="Perfil" component={Perfil} />
        </Stack.Navigator>
        <StorageInfo />
      </SafeAreaView>
    </NavigationContainer>
  );
}
