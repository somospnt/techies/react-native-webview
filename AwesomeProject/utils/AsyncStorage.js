import AsyncStorage from '@react-native-async-storage/async-storage';

const storeData = async (value) => {
    try {
        const jsonValue = JSON.stringify(value)
        console.log(jsonValue);
        await AsyncStorage.setItem('credenciales', jsonValue)
    } catch (e) {
        console.log(e);
    }
};


const getData = async () => {
    try {
        const jsonValue = await AsyncStorage.getItem('credenciales')
        return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
        console.log(e);
    }
};

export { getData, storeData };