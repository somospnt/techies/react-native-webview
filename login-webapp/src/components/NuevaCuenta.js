import React, { useState } from "react";
import { Link } from 'react-router-dom';

const NuevaCuenta = (props) => {

    const [usuario, guardarUsuario] = useState({
        nombre: '',
        email: '',
        password: '',
        confirmar: ''
    });

    const { nombre, email, password, confirmar } = usuario;

    const onChange = event => {
        guardarUsuario({
            ...usuario,
            [event.target.name]: event.target.value
        });
    };

    const mostrarAlerta = (mensaje) => {
        if (window.ReactNativeWebView) {
            window.ReactNativeWebView?.postMessage(JSON.stringify({ event: 'alerta', payload: { mensaje } }));
        } else {
            alert(mensaje);
        }
    };

    const onSubmit = event => {
        event.preventDefault();

        // Validar que no haya campos vacios
        if (
            nombre.trim() === '' ||
            email.trim() === '' ||
            password.trim() === '' ||
            confirmar.trim() === ''
        ) {
            mostrarAlerta('Todos los campos son obligatorios');
            return;
        }
        // Password minimo 6 caracteres
        if (password.length < 6) {
            mostrarAlerta('El password debe ser de al menos 6 caracteres');
            return;
        }
        // Validar ambos passwords sean iguales
        if (password !== confirmar) {
            mostrarAlerta('Los passwords no son iguales');
            return;
        }
        window.ReactNativeWebView?.postMessage(JSON.stringify({ event: 'crearCuenta', payload: { nombre, email, password } }));
    }
    return (
        <div className='form-usuario'>
            <div className='contenedor-form sombra-dark'>
                <h1>Obtener una cuenta</h1>

                <form
                    onSubmit={onSubmit}
                >
                    <div className='campo-form'>
                        <label htmlFor='nombre'>Nombre</label>
                        <input
                            type='text'
                            id='nombre'
                            name='nombre'
                            placeholder='Tu Nombre'
                            value={nombre}
                            onChange={onChange}
                        />
                    </div>

                    <div className='campo-form'>
                        <label htmlFor='email'>Email</label>
                        <input
                            type='email'
                            id='email'
                            name='email'
                            placeholder='Tu Email'
                            value={email}
                            onChange={onChange}
                        />
                    </div>

                    <div className='campo-form'>
                        <label htmlFor='password'>Password</label>
                        <input
                            type='password'
                            id='password'
                            name='password'
                            placeholder='Tu Password'
                            value={password}
                            onChange={onChange}
                        />
                    </div>

                    <div className='campo-form'>
                        <label htmlFor='confirmar'>Confirmar Password</label>
                        <input
                            type='password'
                            id='password'
                            name='confirmar'
                            placeholder='Repite tu Password'
                            value={confirmar}
                            onChange={onChange}
                        />
                    </div>

                    <div className='campo-form'>
                        <input type='submit' className='btn btn-primario btn-block'
                            value='Registrarme' />
                    </div>
                </form>

                <Link to={'/'} className='enlace-cuenta'>
                    Volver a Iniciar Sesión
                </Link>
            </div>
        </div>
    );
};

export default NuevaCuenta;