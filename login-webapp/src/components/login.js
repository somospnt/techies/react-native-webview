import React, { useState, useEffect } from "react";
import { Link, useNavigate } from 'react-router-dom';
import '../index.css';

const Login = (props) => {
    const navigate = useNavigate();

    const [usuario, guardarUsuario] = useState({
        email: '',
        password: ''
    });

    const { email, password } = usuario;

    const onChange = event => {
        guardarUsuario({
            ...usuario,
            [event.target.name]: event.target.value
        });
    };

    const mostrarAlerta = (mensaje) => {
        if (window.ReactNativeWebView) {
            window.ReactNativeWebView?.postMessage(JSON.stringify({ event: 'alerta', payload: { mensaje } }));
        } else {
            alert(mensaje);
        }
    };

    const onSubmit = async (event) => {
        event.preventDefault();

        // Validar que no haya campos vacios
        if (email.trim() === '' || password.trim() === '') {
            mostrarAlerta('Todos los campos son obligatorios');
        } else {
            window.ReactNativeWebView?.postMessage(JSON.stringify({ event: 'login', payload: { email, password } }));
        }
    };

    return (
        <div className='form-usuario'>
            <div className='contenedor-form sombra-dark'>
                <h1 className='titulo'>Iniciar Sesión</h1>

                <form
                    onSubmit={onSubmit}
                >
                    <div className='campo-form'>
                        <label htmlFor='email'>Email</label>
                        <input
                            type='email'
                            id='email'
                            name='email'
                            placeholder='Tu Email'
                            value={email}
                            onChange={onChange}
                        />
                    </div>

                    <div className='campo-form'>
                        <label htmlFor='password'>Password</label>
                        <input
                            type='password'
                            id='password'
                            name='password'
                            placeholder='Tu Password'
                            value={password}
                            onChange={onChange}
                        />
                    </div>

                    <div className='campo-form'>
                        <input type='submit' className='boton-usuario btn-primario btn-block'
                            value='Iniciar Sesión' />
                    </div>
                </form>

                <Link to={'/nueva-cuenta'} className='enlace-cuenta'>
                    Obtener Cuenta
                </Link>
            </div>
        </div>
    );
};

export default Login;