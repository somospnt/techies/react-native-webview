import React, { useEffect } from "react";
import "./App.css";
import Login from "./components/login.js";
import { Routes, Route, useNavigate } from 'react-router-dom';
import NuevaCuenta from "./components/NuevaCuenta";

function App() {
  const navigate = useNavigate();
  useEffect(() => {
    function handleEvent(message) {
      if (message.data.event === 'redirect') {
        navigate(message.data.payload);
      }
    }
    document.addEventListener("message", handleEvent);
    return () => document.removeEventListener("message", handleEvent);
  }, []);

  return (
    <Routes>
      <Route exact path="*" element={<Login />} />
      <Route exact path="/nueva-cuenta" element={<NuevaCuenta />} />
    </Routes>
  );
}

export default App;
